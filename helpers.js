/******************************************************************

										GENERAL HELPER FUNCTIONS

These functions can be used where needed. They are general purpose
and are not directly connected with database/model -management,
controller functionality or view functionality. However they may be
used as part of any of them.

*******************************************************************/


/*
This function generates a random, 12 characters long string
This is used to uniquelly name images when uploaded
https://gist.github.com/gordonbrander/2230317
*/

exports.generateId = function(){
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return '_' + Math.random().toString(36).substr(2, 12);
};


/*
This function moves uploaded files from the temp-file storage to
the final storage path.
*/
var fs = require('fs');

exports.moveUploadedFile = function(originalFilePath, fileDir, fileName){
  var appPath = require('path').dirname(require.main.filename);

  var source = fs.createReadStream(originalFilePath);
  var target = fs.createWriteStream(appPath + fileDir + fileName);

  source.pipe(target);
  source.on("end", function() {
    console.log("Copied succesfully!");
  });
  source.on("error", function() {
    console.log("error");
  });
};

// A function to check user input on registration and update.
exports.checkRegisterInput = function(registerInfo) {

  // A name must be at least three characters long.
  var usernameRegEx = /^[A-Za-z0-9_-]{3,31}$/;
  var longNameRegEx = /^[A-Za-z0-9_-\s]{3,31}$/;
  var emailRegEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  // A password must be at least eight characters long, contain one upper case letter, lower case
  // letter and number.
  var passwordRegEx = /(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z!@#$%^&*()]*$/;
  // Street addresses must have the form <street name> <building number>
  var streetaddressRegEx = /^((.){1,}(\d){1,}(.){0,})$/;
  // Zip codes are five numbers long.
  var zipcodeRegEx = /^\d{5}$/;
  var cityRegEx = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/;
  var countryRegEx = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/;
  // A phone number is five to ten digits long.
  var phoneRegEx = /^\d{5,10}$/;

  var testsPassed = true;

  // For easier debugging, comment these if-clauses out.
  // Checking mandatory fields.
  if (!usernameRegEx.test(registerInfo.username) || !emailRegEx.test(registerInfo.email) ||
      !passwordRegEx.test(registerInfo.password) ||
      registerInfo.password !== registerInfo.passwordConfirmed) {

    testsPassed = false;
  }
  // Checking additional input if the user registers as a restaurant.
  else if (registerInfo.isRestaurant && (!longNameRegEx.test(registerInfo.longname) ||
      !streetaddressRegEx.test(registerInfo.street_address) ||
      !zipcodeRegEx.test(registerInfo.postal_code) || !cityRegEx.test(registerInfo.city) ||
      !countryRegEx.test(registerInfo.country) || !phoneRegEx.test(registerInfo.phonenumber))) {

    testsPassed = false;
  }
  // Checking additional input if a non-restaurant user has entered text.
  else if (!registerInfo.isRestaurant &&
      ((registerInfo.longname !== "" && !longNameRegEx.test(registerInfo.longname)) ||
      (registerInfo.street_address !== "" && !streetaddressRegEx.test(registerInfo.street_address)) ||
      (registerInfo.postal_code !== "" && !zipcodeRegEx.test(registerInfo.postal_code)) ||
      (registerInfo.city !== "" && !cityRegEx.test(registerInfo.city)) ||
      (registerInfo.country !== "" && !countryRegEx.test(registerInfo.country)) ||
      (registerInfo.phonenumber !== "" && !phoneRegEx.test(registerInfo.phonenumber)))) {

    testsPassed = false;
  }

  return testsPassed;
};