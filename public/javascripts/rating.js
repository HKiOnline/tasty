/*
	This is the client script for rating dishes
*/

$( document ).ready(function() {
	
	// Hide elements used only when there is no js available
	$(".no-script-rating").hide();
	
	// 
	$(".rateit").bind('rated', function (event, value) {
		$("#ratingValue").val(value);
	});
	
	
	$("#review-form").submit(function( event ) {
		// When submit button is pressed, prevent normal functionality
		event.preventDefault();	
  	
		// Collect all review information
		var review = {
			dish: $("#dishId").val(),
			rating: $("#ratingValue").val(),
			text: $("#reviewText").val()
		};
		
		// Make an ajax call and send the review as JSON
		$.ajax({
		  url: "/dish/"+$("#dishId").val()+"/review",
		  type: "POST",
		  contentType: "application/json; charset=utf-8",
		  dataType: "json",
		  data: JSON.stringify({ review: review })
		}).done(function() {
			
			// When the JSON is sent successfully we reload the page
			location.reload(true);
		});

	});
	
	

	
});

