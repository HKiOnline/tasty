/*
	Require all route controllers
*/

var user = require('./controllers/user');
var dish = require('./controllers/dish');
var review = require('./controllers/review');
var search = require('./controllers/search');

module.exports = function (app) {

	// Route for the front page, this could be also dish.all
	app.get('/', dish.index);

	// Route for login
	app.get('/login', user.login); // Show the login form
	app.post('/login', user.login); // Handle login information

	// Route for logout
	app.get('/logout', user.logout);

	// Route for registering as a new user (either as reviewer, restaurant or as admin)
	app.get('/register', user.register); // Show the registration form
	app.post('/register', user.register); // Handle registration information
	app.post('/dashboard', user.update); // Handle when updating user profile information
	app.post('/dashboard/handleactivate', user.handleActivation); // Admin user can activate/inactivate restaurant user

	// Routes for user resources, users see a "dashboard"
	app.get('/dashboard/users', user.all); // show all users (for admins)
	app.get('/dashboard', user.show); // show the logged in users info
	//app.get('/dasboard/getUserData', user.showUserData); // show the logged in users infoesi
	app.post('/dashboard/updatePassword', user.updatePassword);  //updates users password
	app.post('/dashboard/passwordRecent', user.checkRecentPassword);  //updates users password
	app.get('/profile/:id', user.profile); // Show selected users profile
	app.get('/dashboard/getreview/:id', user.getReview); // Show selected review
	app.post('/dashboard/editreview', user.editReview); // Update review
	app.get('/users', user.all); // Show registered users for admin


	// Routes for search
	app.post('/search', search.search);
	app.get('/search/:keyword', search.search_keyword);

	// Routes for dishes
	app.get('/dishes', dish.index); // List all dishes
	app.get('/dish', dish.index);	// List all dishes

	// Routes for creating a dish
	app.get('/dish/new', dish.create); // Show a Form to create a new dish
	app.post('/dish/new', dish.create); // Create a new dish
	app.post('/dish', dish.create); // Create a new dish, this is for more REST oriented clients

	// Routes for showing a dish
	app.get('/dish/:id', dish.show); // Show one dish

	// Routes for editing a dish
	app.get('/dish/:id/edit', dish.update); // Show a Form to update an existing dish
	app.post('/dish/:id/edit', dish.update); // Update an existing dish
	app.put('/dish/:id', dish.update); // Update an existing dish

	// Routes for deleting a dish
	app.get('/dish/:id/delete', dish.delete); // Delete an existing dish
	app.delete('/dish/:id', dish.delete); // Delete an existing dish, this is for those clienst that do REST

	// Routes for reviewing a dish
	app.post('/dish/:id/review', review.create); // Delete an existing dish

	// Routes for restaurants
	app.get('/restaurants', user.allRestaurants); // List all restaurants
	app.get('/restaurant', dish.all);	// List all restaurants

	app.get('/restaurant/:id', dish.show); // Show one restaurant

	// Needs form routes for restaurants

	// We may need routes for the reviews or maybe we can use sockets
	
};