/*
	This is a controller for the search.
	Things that could be searched are dishes and users. If possible, both,
	but first and foremost dishes.
*/


/* 
	This function handles the search form input where ever the form might be.
	Then it searches the database and displays the results.
*/

var models = require('../database');

exports.search = function(req, res){
  renderSearch(req,res);	
 	
};

exports.search_keyword = function(req, res){
  renderSearchKeyword(req,res);  	
};

var renderSearch = function(req,res){
  
  models.Dish.textSearch(req.body.search, function (err, output) {

        if (err) 
 	{
 		console.log(err);
            	res.send('Something wrong with the database');
        }
        else
        {   
        	if(output.stats.nscanned == 0) {
                     console.log(output);
                     res.render('search/nomatches',{result:req.body.search});
                }
                else {
                     // console.log(output.results);
   		     res.render('search/search',{dishes:output.results, result:req.body.search});
   		}
        }
   });	
}

var renderSearchKeyword = function(req,res){
  
  models.Dish.textSearch(req.param("keyword"), function (err, output) {

        if (err) 
 	{
 		console.log(err);
            	res.send('Something wrong with the database');
        }
        else
        {   
        	if(output.stats.nscanned == 0) {
                     console.log(output);
                     res.render('search/nomatches',{result:req.body.search});
                }
                else {
                     // console.log(output.results);
   		     res.render('search/search',{dishes:output.results,result:req.param("keyword")});
   		}
        }
   });	
}