var models = require('../database');
var crypto = require('crypto');
var fs = require('fs');
var os = require('os');
var helper = require('../helpers');


var database = require("../database");

/*
 * GET users listing.
 */

exports.handleActivation = function(req, res) {

    if(typeof req.session.user !== "undefined" && req.session.level === 3) {

        models.User.update({_id:req.body.id}, {$set : {active:req.body.active}}, function(err, numberAffected) {
        if(err) {

        }
        else if(numberAffected == 1) {
          res.send({status:"SUCCESS"});
        }

        });
    }
    else {
      res.redirect("/");
    }

};
// Get user information of all users
exports.allRestaurants = function(req, res){

     models.User.find({level:{$in:[2]}}).sort({added:"desc"})
     .exec(function(err, users) {
        if(err) {
          throw err;
        }
        else {
        	console.log(users);
	  res.render("user/userlist", {	userdata: users	});
	}
    });
};

// Get user information of all users
// This is for administrators only
exports.all = function(req, res){
	console.log("Show users: "+req.session.user);
  if(typeof req.session.user !== "undefined" && req.session.level === 3) {

     models.User.find({level:{$in:[1,2]}}).sort({added:"desc"})
     .exec(function(err, users) {
        if(err) {
        	throw err;
        }
        else {

        	models.Dish.find().sort({points: "desc"}).limit(10).exec(function (err, topDishes) {
				if (err){
					throw err;
				}else{

				models.Dish.find().sort({added: "desc"}).limit(10).populate('restaurant')
				.exec(function (err, newDishes) {
					if (err){
						throw err;
					}else{

				    res.render("user/index", {
								topDishes: topDishes,
								newlyAddedDishes: newDishes,
								userdata: users
							});
				}
			});
		}
	});

        }

     });

  }

  else
  	res.redirect("/");
};

exports.updatePassword = function(req, res) {
    console.log("Update userdata");

    var newSalt = crypto.randomBytes(44).toString("base64");
    var hash = crypto.createHash("sha256").update(req.body.password + newSalt, "utf8").digest("base64");

    models.User.update({_id:req.session.user}, {password:hash, password_salt: newSalt}, function(err, numberAffected) {
      if(err) {
        console.log(err);
      }
      else if(numberAffected == 1) {
       	res.send({status:"SUCCESS"});
      }

    });
};

// Get user information of a single user
// Every user gets to see their own info

exports.show = function(req, res){

    if(typeof req.session.user !== "undefined") {
    console.log("Show user's data");

    models.User.findOne({_id:req.session.user}, function(err,user) {

          if(user != null) {

          	if(user.level === 1) {
      	  		models.Review.find({'user':user._id}).sort({rating:"desc"})
      	  		.populate('dish')
      	  		.exec(function(err, review) {
          			if(err) {
            			console.log("Error fetching reviews");
          			}
          			else {
          				models.Dish.find().sort({added: "desc"}).limit(10).exec(function (err, newDishes) {
							if (err) {
								throw err;
							}
							else {
      	      					//console.log(review);
      	      					res.render("user/show", {userdata:user, reviewdata:review, latestDishes:newDishes, mode:'UPDATE'});
      	      				}
   	      				});
   	      			}
   		   		}
   		    )}
   		   	else {
   		   		models.Dish.find({'restaurant':user._id}).sort({added:"desc"}).limit(10)
   		   		.exec(function(err, dishes) {
          			if(err) {
          				throw err;
          			}
          			else {

          				models.Review.find({'dish':{$in: dishes}}).sort({added:"desc"}).limit(10)
          				.populate('user')
          				.populate('dish')
          				.exec(function(err, data) {
          					if(err) {
          						throw err;
          					}
          					else {
          					    res.render("user/show", {userdata:user, latestDishes:dishes, reviewdata:data, mode:'UPDATE'});
          					}
          				});
          			}

          		});
   		   	}
   	  	  }
   	  });
	}
	else {

		res.redirect("/");
	}
};

exports.getReview = function(req, res) {

    console.log("Get review "+req.param('id'));

    models.Review.findOne({'_id':req.param('id')})
    .exec(function(err, review) {

    	if(err) {
    		throw err;
    	}
    	else {
    		if(review) {
   				res.render("review/show", {reviewdata:review});

    		}
    		else {
    			throw err;
    		}

    	}
    })



};

exports.editReview = function(req, res) {

    console.log("Edit review "+req.body.review.rid);

    models.Review.findOne({'_id':req.body.review.rid})
    .populate('user')
    .exec(function(err, review) {

    	if(err) {
    		throw err;
    	}
    	else {
    		if(review) {

    			models.Review.update({'_id':review._id}, {$set : {text:req.body.review.text, rating:req.body.review.rating, updated:new Date()}}, function(err, numberAffected) {
        				if(err) {
        					throw err;
        				}
        				else if(numberAffected === 1) {
        					res.send({status:"SUCCESS"});
        				}

    				});
    		}
    		else {
    			res.send({status:"ERROR"});
    		}

    	}
    })



};

exports.profile = function(req, res) {

    console.log("Get user "+req.param('id'));

    models.User.findOne({'_id':req.param('id')})
    .exec(function(err,data) {
    	if(err) {
    		throw err;
    	}
    	else {

    		if(data !== null) {

    			models.Review.find({user:data._id}).sort({added:"desc"}).limit(10)
    			.populate('dish')
    			.exec(function(err, reviews) {
    				res.render("user/profile", {userdata:data, reviewdata:reviews});
    			});

    		}
    		else {

    			console.log("Couldn't find user with id: "+req.param('id'));

    			res.render("user/profile");
    		}
    	}

    })


};

exports.checkRecentPassword = function(req, res) {

	console.log(req.session.user);
    models.User.findOne({'_id':req.session.user})
   .exec(function(err, data) {
      if(err) {
          console.log("Error fetching userdata");
      }
      else {
          if(data !== null) {
              var hashed = crypto.createHash("sha256").update(req.body.password + data.password_salt, "utf8").digest("base64");
             if(hashed != data.password) {

                 res.send({status:"ERROR"});
             }
             else {
             	 res.send({status:"SUCCESS"});
             }
          }
          else {

             throw err;
          }


      }

  });

};

exports.update = function(req, res) {
  models.User.findOne({"_id": req.session.user}, function(error, user) {
    if (error) {
      res.send(error);
    }
    else if (user === null) {
      res.send("User not found.");
    }
    else {
      // Update the users longname. Other basic fields are not updatable.
      user.longname = req.body.fullname;

      //Update contact information fields.
      user.street_address = req.body.streetaddress;
      user.postal_code = req.body.zipcode;
      user.city = req.body.city;
      user.country = req.body.country;
      user.phonenumber = req.body.phone;

      // Update the users photo if a new one has been uploaded.
      if (req.files.image.size > 0) {
        // First delete the users old photo.
        fs.unlink(require('path').dirname(require.main.filename) + "/public/images/users/" +
            user.photopath, function(error) {

          if (error) {
            console.log("Failed to unlink: " + user.photopath);
          }
        });

        // Generate a new photopath for the user.
        user.photopath = "user_img" + helper.generateId() + "." +
            req.files.image.type.split("/")[1];

        // Move the file into the /public/images/users folder.
        helper.moveUploadedFile(req.files.image.path, "/public/images/users/", user.photopath);
      }

      user.save(function(error) {
        if (error) {
          res.send(error);
        }
        else {
          res.redirect("/dashboard");
        }
      });
    }
  });
}

var clearFields = function(req, res) {


	newUser = new models.User();
	newUser.username = "";
    newUser.email = "";
    newUser.longname = "";
    newUser.street_address = "";
    newUser.postal_code = "";
    newUser.city = "";
    newUser.phonenumber = "";

    return newUser;
}

// This is for login in
exports.login = function(req, res){
  if (req.method === "GET") {
    res.render("user/login");
  }
  else if (req.method === "POST") {
    var loginInfo = {username: req.param("username"),
                    password: req.param("password")};

    // Get salt for the user.
    models.User.findOne({username: loginInfo.username}, "password_salt", function(error, user) {
      if (error) {
        console.log("Database error!");
        res.render("user/login");
      }
      else if (user === null) {
        console.log("User not found!");
        res.render("user/login");
      }
      else {
        loginInfo.password = crypto.createHash("sha256")
            .update(loginInfo.password + user.password_salt, "utf8").digest("base64");

        models.User.findOne({$and: [{$or: [{"username": loginInfo.username}, {"email": loginInfo.username}]},
            {"password": loginInfo.password}]}, "_id level username", function(error, user) {

          if (error) {
            res.send(error);
          }
          else if (user === null) {
            res.render("user/login");
          }
          else {
            req.session.user = user._id;
            req.session.username = user.username;
            req.session.level = user.level;
            res.redirect("/");
          }
        });
      }
    });
  }
  else {
    res.redirect("/");
  }
};

// A function for logging the user out from the current session.
exports.logout = function(req, res) {
  req.session.destroy();

  res.redirect("/");
}

//This is for registring as a reviewer or as a restaurant
exports.register = function(req, res){

  if (req.method === "GET" && typeof req.session.user === "undefined") {

  	var newUser = clearFields();
    res.render("user/create", {userdata:newUser, mode:'CREATE'});
  }
  else if (req.method === "POST") {
    var registerInfo = {username: req.body.username,
                        longname: req.body.fullname,
                        email: req.body.email,
                        password: req.body.password,
                        passwordConfirmed: req.body.password_confirmation,
                        photopath: "",
                        street_address: req.body.streetaddress,
                        postal_code: req.body.zipcode,
                        city: req.body.city,
                        country: req.body.country,
                        phonenumber: req.body.phone};

    if (req.files.image.size > 0) {
      var contentType = req.files.image.type.split("/")[1];
      registerInfo.photopath = "user_img" + helper.generateId() + "." + contentType;
      console.log(registerInfo.photopath);
    }

    var timeAdded = new Date();
    timeAdded.toISOString();
    registerInfo.added = timeAdded;

    // Restaurant accounts are not active immediately after creation. The user level is also set
    // accordingly.
    if (req.body.restaurant) {
      registerInfo.isRestaurant = true;
      registerInfo.active = false;
      registerInfo.level = 2;
    }
    else {
      registerInfo.isRestaurant = false;
      registerInfo.active = true;
      registerInfo.level = 1;
    }

    registerUser(req, res, registerInfo);
  }
  else {
    res.redirect("/");
  }
};

// The actual function where user info is saved.
var registerUser = function(req, res, registerInfo) {

  if (helper.checkRegisterInput(registerInfo)) {
    // Create salt for user password.
    registerInfo.password_salt = crypto.randomBytes(44).toString("base64");

    registerInfo.password =
        crypto.createHash("sha256")
        .update(registerInfo.password + registerInfo.password_salt, "utf8").digest("base64");

    var newUser = new models.User(registerInfo);

    newUser.save(function(error) {
      if (error) {
        res.send(error);
      }
      else {
        // Find created user from database and store user id in the session.
        models.User.findOne({"username": registerInfo.username,
            "password": registerInfo.password}, "_id level username", function(error, user) {

          if (error) {
            res.send(error);
          }
          else {
            req.session.user = user._id;
            req.session.username = user.username;
            req.session.level = user.level;

            if (req.files.image.size > 0) {
              helper.moveUploadedFile(req.files.image.path, "/public/images/users/",
                  registerInfo.photopath);
            }

            res.redirect("/");
          }
        });
      }
    });
  }
  else {
    res.send("Registration failed.");
  }
}