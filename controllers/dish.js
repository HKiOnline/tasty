var models = require('../database');
var helper = require('../helpers');

// A limited set of dishes for e.g. a front page
exports.index = function(req, res){
	
	// This horrifying monster takes all dishes, calculates rating and sorts them, limit is 10
	// If you know a better way for doing this, a less monstrous way, let me know (Henri).
	models.Dish.find().sort({rating: "desc", points: "desc"}).limit(10).populate("restaurant").exec(function (err, topDishes) {
		if (err){
			throw err;
		}else{

			models.Dish.find().sort({added: "desc"}).limit(10).populate("restaurant").exec(function (err, newDishes) {
				if (err){
					throw err;
				}else{
					models.Dish.find().where("highlighted").equals(true).sort({updated: "desc"}).limit(5).populate("restaurant").exec(function (err, highlightDish) {
						if (err){
							throw err;
						}else{
										
							res.render("dish/index", {
								title: "Tasty Dish Reviews", 
								topDishes: topDishes, 
								newlyAddedDishes: newDishes, 
								highlightDish: highlightDish[0]
							});
						}
					});
				}
			});
		}
	});	
};

// Get information of all dishes
exports.all = function(req, res){
  res.render("dish/index");
};

// Get user information of a single user
// Every dish has it's own info page
exports.show = function(req, res){
		
	// First we try to find the dish with the id
    models.Dish.findOne({'_id':req.param('id')}).populate('restaurant').exec(function(err,dish){
     if(err){
       	  res.redirect("/");
     }
	   else{ // if we have our dish, then we will find it's reviews if available
		 	if(dish){   
			   models.Review.find().where("dish").equals(dish._id).populate('user').sort({added: "desc"}).exec(function (err, reviews){
					 if(err){
						 throw err;
					 }
				   else{
						 
						 // Check if the user has all ready given a review for this dish
						 var userHasReviewed = false;
						 if(req.session.user){
							 
							 reviews.some(function(review, index, array){
								 
								 if(review.user.id == req.session.user){
								 	userHasReviewed = true;	
									return true;
								 }
							 });	
						 }
						 
						 // Check if we have any reviews
						 if (reviews.length == 0) {
							reviews = null;
						 }
				   	
						res.render('dish/show',{title: "Tasty "+dish.name, dish:dish, reviews:reviews, userHasReviewed: userHasReviewed});
				   }
			   });
			   
			 }
		   else {
				 res.redirect("/");
		   }
	   } 
	 });
};

// Create and save a new dish
exports.create = function(req, res){
    
	// If the request method is GET then just display empty HTML-form
	if (req.method === "GET" && typeof req.session.user !== "undefined" && req.session.level >= 2){
		    
	  res.render("dish/create", {title: "Create a new dish"});
	  
  } // If the request method is POST then we need to handle form data
	else if(req.method === "POST" && typeof req.session.user !== "undefined" && req.session.level >= 2){
		                
    models.User.findOne({_id:req.session.user}, function(err,user) {
		
			// We capture the dish information to dish object
			
			var keywords = req.body.keywords.split(',');
			
			var dishFormData = {
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				type: req.body.type,
				keywords: keywords,
				restaurant: req.session.user
			};

			dishFormData.keywords.push(user.longname);
			dishFormData.keywords.push(user.city);


		
			if(req.files.photo.size > 0){
			
				var contentType = req.files.photo.type.split("/")[1];
			
				dishFormData.photopath = "dish_img"+helper.generateId()+"."+contentType;
				console.log(dishFormData.photopath);
			}

			// In here we save the new dish into the database
			var newDish = new models.Dish(dishFormData);
		
		    newDish.save(function(error) {
		      if (error) {
						// If there is an error we log it and notify the user
						console.log(error);
		        res.render("dish/create", {title: "Create a new dish"});
		      }
		      else {
					
						// Next we will save the uploaded file, if there was one
						if(req.files.photo.size > 0){
							helper.moveUploadedFile(req.files.photo.path, "/public/images/dishes/", dishFormData.photopath);
						}
					
						// If all goes well we will redirect to the dish/show-view
						res.redirect("/dish/"+newDish._id);
		      }
		    });
		});
	}
	else{
		res.redirect("/");
	}
};

// Update an existing dish
exports.update = function(req, res){
	
	// If the request method is GET then just display empty HTML-form
	//if (req.method === "GET" && typeof req.session.user !== "undefined" && req.session.level >= 2){
	if (req.method === "GET"){
	
			models.Dish.findOne({'_id':req.param('id')}, function(err,dish){
	      if(err){
					res.redirect("/");
				}
				else{	
					res.render("dish/update", {title: "Update dish", dish: dish});
				}
			});
	}else if(req.method === "POST"){
            
    models.User.findOne({_id:req.session.user}, function(err,user) {		
		
		var keywords = req.body.keywords.split(',');
		
		var dish = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			type: req.body.type,
			keywords: keywords,
			restaurant: req.session.user
		};
		
		dish.keywords.push(user.longname);
		dish.keywords.push(user.city);
		
		if(req.files.photo.size > 0){
		
			var contentType = req.files.photo.type.split("/")[1];
		
			dish.photopath = "dish_img"+helper.generateId()+"."+contentType;
			console.log(dish.photopath);
		}
	
	  models.Dish.update({'_id':req.param('id')}, dish, function(error) {
	    if (error) {
				// If there is an error we log it and notify the user
				console.log(error);
	      res.render("dish/update", {title: "Update dish"});
	    }
	    else {
			
				// Next we will save the uploaded file, if there was one
				if(req.files.photo.size > 0){
					helper.moveUploadedFile(req.files.photo.path, "/public/images/dishes", dish.photopath);
				}
			
				// If all goes well we will redirect to the dish/show-view
				res.redirect("/dish/"+req.param('id'));
	    }
          });
	
	});
		
	}else{
		console.log("Method used: "+req.method);
		console.log(req.body);
		res.redirect("/");
	}
};

// Deletes an existing dish
exports.delete = function(req, res){
  res.send("Delete not yet implemented");
};



