# Tasty - The Dish Rating App
And by dish we mean the edible kind, not the "point in the sky and receive alien communications" kind.

Tasty is a web application for rating restaurant meals and menu items.

Make sure you have Node installed. After you have cloned the project cd into the project directory and run "npm install" to install all other dependencies, start the app with "node app.js" and head to [http://localhost:8080](http://localhost:8080).


## Running The App For The First Time

1. Install [NodeJS](http://nodejs.org)
2. Install [MongoDB](https://www.mongodb.org)
2. Install [Git](http://git-scm.com) 
3. Clone the project `git clone https://HKiOnline@bitbucket.org/HKiOnline/tasty.git`
4. Navigate to the project directory `cd tasty`
5. Run `npm install` to download and install all dependencies
6. Start MondoDB instance `mongo --dbpath /path/to/the /db/dir --setParameter textSearchEnabled=true`
7. Run `mongo localhost:27017/tastydb seed.js` to seed the database
8. Run `node app.js`

_Mac users:_ You might want to try [homebrew](http://brew.sh). You can install node, mongo and git right from your terminal with a single line.

`brew install git nodejs mongodb`

## Project Structure

- app dependencies are handled at package.json -file and npm
- database-settings and configuration is done in database.js
- context routing is done in router.js
- route controllers and their logic are in the controller-directory
- views are created with jade at view-directory, each controller has it's own directory
- styling is done with stylus at public/stylesheets-directory, please don't use inline CSS

## Seeding the Database

The project includes a seed.js file that can be used to seed the database. There are some sample dishes there, users and reviews.

To seed the database:

1. Start MongoDB `mongo --dbpath /path/to/the /db/dir --setParameter textSearchEnabled=true`
2. Navigate to the project directory
3. Run `mongo localhost:27017/tastydb seed.js`

## Authors

This project was created as a collaboration. The original authors are listed below, all rights are reserved to them, but if you wish to use the code ask any of the authors. The authors may use the code in any way they see fit including publish it as open source on a later date.

- [Henri Kesseli](https://bitbucket.org/HKiOnline)
- [Jarno Maimonen](https://bitbucket.org/jarno_maimonen)
- [Timo Väänänen](https://bitbucket.org/Alexander-73)
- [Ville Pylkki](https://bitbucket.org/VilleP)
