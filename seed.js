/*
	Database Seed File
	*************************************
	
	To seed the database:
		1. Start MongoDB
		2. Navigate to the project directory
		3. Run "mongo localhost:27017/tastydb seed.js"	

*/

// Clear out old data and re-initialize each time the program runs
db.dropDatabase();

	// All users have the password Salasana1
	var users =[
	 {
		 _id : ObjectId("5321b9287bbf2ea6f8c45521"),
		 username: "reviewer",
		 longname: "Ron Reviewer Jr.",
		 phonenumber: "555-75757",
		 street_address: "J.R. Ewing Street",
		 postal_code: 22277,
		 city: "Dallas",
		 country:"United States",
		 password:"AluZMtV8qzwOP3XyxyjFzkrO8IoZmOCkexsROqGNn1A=",
		 password_salt: "5kKY1w5M+Tw9G/v5PSHr1vnqmtXnmSEXac7qu3oliJsKQIdgPrcidbqEQG0=",
		 email:"ron@test.us",
		 photopath:"prototype_user.jpg",
		 active: true,
		 level:1
	},
	{
			_id: ObjectId("531ee47e6e8942592fcb257c"),
			username: "restaurant",
			longname: "Mixed Bag of Surprises",
			phonenumber: "555-75757",
			street_address: "Ruokalanraitti 666",
			postal_code: 22277,
			city: "Tampere",
			country:"Finland",
 		 	password:"AluZMtV8qzwOP3XyxyjFzkrO8IoZmOCkexsROqGNn1A=",
 		 	password_salt: "5kKY1w5M+Tw9G/v5PSHr1vnqmtXnmSEXac7qu3oliJsKQIdgPrcidbqEQG0=",
			email:"contanc@bag.fi",
			photopath:"prototype_user.jpg",
			active: true,
			level:2
	},
	{
			_id : ObjectId("5321b9287bbf2ea6f8c45522"),
			username: "admin",
			longname: "Alpo Daniel Minkki",
			phonenumber: "555-75757",
			street_address: "Ylikäyttäjänvaltaväylä 1",
			postal_code: 22277,
			city: "Tampere",
			country:"Finland",
 		 	password:"AluZMtV8qzwOP3XyxyjFzkrO8IoZmOCkexsROqGNn1A=",
 		 	password_salt: "5kKY1w5M+Tw9G/v5PSHr1vnqmtXnmSEXac7qu3oliJsKQIdgPrcidbqEQG0=",
			email:"contanc@bag.fi",
			photopath:"prototype_user.jpg",
			active: true,
			level:3
	}
	];

	var dishes = [
		{ 
			_id : ObjectId("5321a6c12db04d10d1b7859f"),
			name : "Hernekeitto",
			description : "This classical Finnish delicasy has been prepared from the very best canned beans possible. A perfect lunch for vegetarians since there has been only one recorded insident since 1887 where a customer has reported that they have actually found some meat from a canned pea soup.",
			price : 10,
			type : "Soup",
			photopath : "hernekeitto.jpg",
			restaurant :ObjectId("531ee47e6e8942592fcb257c"),
			updated : ISODate("2014-03-13T12:38:25.131Z"),
			added : ISODate("2014-03-13T12:38:25.131Z"),
			highlighted : false,
			reviews : 20,
			points : 80,
			rating: 4,
			keywords : [ "Mixed Bag of Surprises","Tampere","soup"],
			__v : 0 
		},
		{ 
			name : "Borscht",
			description : "Borscht is popular in many Eastern and Central European countries. In most of these countries, it is made with beetroot as the main ingredient. In some countries, tomato is used as the main ingredient, while beetroot acts as a secondary ingredient.",
			price : 12,
			type : "Soup",
			photopath : "prototype_burger.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321ad766f48c033d6081e77"),
			updated : ISODate("2014-03-13T13:07:02.907Z"),
			added : ISODate("2014-03-13T13:07:02.907Z"),
			highlighted : false,
			reviews : 30,
			points : 90,
			rating: 3,
			keywords : [ "Mixed Bag of Surprises","Tampere","Soup"],
			__v : 0 
		},
		{ 
			name : "Pizza",
			description : "Yummu pizza with thin crust, delicious tomato sauce , minced meat, cheese, onions and olives.",
			price : 8,
			type : "Elixir",
			photopath : "prototype_burger.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321aee16f48c033d6081e78"),
			updated : ISODate("2014-03-13T13:13:05.685Z"),
			added : ISODate("2014-03-13T13:13:05.685Z"),
			highlighted : false,
			reviews : 20,
			points : 100,
			rating: 5,
			keywords : [ "Mixed Bag of Surprises","Tampere","pizza", "meat", "olives", "onions"],
			__v : 0 
		},
		{
			name : "Meatballs With Mashed Potatoes",
			description : "A scandinavian speciality. Always delicious, always meaty, alway ballsy.",
			price : 12,
			type : "Elixir",
			photopath : "prototype_burger.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b0a7cb7cdbb1dbf20277"),
			updated : ISODate("2014-03-13T13:20:39.576Z"),
			added : ISODate("2014-03-13T13:20:39.575Z"),
			highlighted : false,
			reviews : 21,
			points : 105,
			rating: 5,
			keywords : [ "Mixed Bag of Surprises","Tampere","Mashed", "potatoes", "Meat"],
			__v : 0 
		},
		{ 
			name : "Smoked Salmon",
			description : "Delicious salmon, caught from the wild where fish did not see its demise coming and was perfectly happy swimming and drinking water.",
			price : 4,
			type : "Elixir",
			photopath : "prototype_burger.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b297ea4b9b2bde723c2e"),
			updated : ISODate("2014-03-13T13:28:55.773Z"),
			added : ISODate("2014-03-13T13:28:55.773Z"),
			highlighted : false,
			reviews : 10,
			points : 40,
			rating: 4,
			keywords : [ "Mixed Bag of Surprises","Tampere","salmon", "fish", "seafood"],
			__v : 0
		},
		{ 
			name : "Mustamakkara",
			description : "This traditional black sausage hails from the region of Tampere. It divides people in to two camps immediately when people see the food and hears the ingredient.",
			price : 8,
			type : "Elixir",
			photopath : "mustamakkara.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b3d9ea4b9b2bde723c2f"),
			updated : ISODate("2014-03-13T13:34:17.242Z"),
			added : ISODate("2014-03-13T13:34:17.242Z"),
			highlighted : true,
			reviews : 40,
			points : 120,
			rating: 3,
			keywords : [ "Mixed Bag of Surprises","Tampere","sausage"],
			__v : 0 
		},
		{ 
			name : "Bread cheese",
			description : "Bread cheese (Leipäjuusto, fin.) is a tasty and squeaky side dish for lunches or with caffee.",
			price : 2,
			type : "Elixir",
			photopath : "leipäjuusto.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b5b1a1239ed6e06eb383"),
			updated : ISODate("2014-03-13T13:42:09.944Z"),
			added : ISODate("2014-03-13T13:42:09.944Z"),
			highlighted : false,
			reviews : 60,
			points : 180,
			rating: 2,
			keywords : [ "Mixed Bag of Surprises","Tampere","leipajuusto", "bread cheese", "dessert"],
			__v : 0
		},
		{ 
			name : "Whipped Lingonberry Porridge",
			description : "Whipped lingonberry porridge (Vispipuuro, fin.) is a traditional Finnish breakfast and evening dish. Simple to make and delicious.",
			price : 4,
			type : "Elixir",
			photopath : "vispipuuro.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b650a1239ed6e06eb384"),
			updated : ISODate("2014-03-13T13:44:48.838Z"),
			added : ISODate("2014-03-13T13:44:48.838Z"),
			highlighted : false,
			reviews : 12,
			points : 48,
			rating: 4,
			keywords : [ "Mixed Bag of Surprises","Tampere","porridge"],
			__v : 0
		},
		{ 
			name : "Salmon Soup",
			description : "A salmon, leek and potato soup, usually garnished with dill.",
			price : 7,
			type : "Soup",
			photopath : "prototype_burger.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b6a7a1239ed6e06eb385"),
			updated : ISODate("2014-03-13T13:46:15.297Z"),
			added : ISODate("2014-03-13T13:46:15.297Z"),
			highlighted : false,
			reviews : 11,
			points : 44,
			rating: 4,
			keywords : [ "Mixed Bag of Surprises","Tampere","salmon", "soup"],
			__v : 0 
		},
		{ 
			name : "Cabbage Rolls",
			description : "Steamed cabbage leaves stuffed with beef, onions and spices. Typically served with lingonberry jam.",
			price : 8,
			type : "Casserol",
			photopath : "kaalikääryleet.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b6e2a1239ed6e06eb386"),
			updated : ISODate("2014-03-13T13:47:14.464Z"),
			added : ISODate("2014-03-13T13:47:14.464Z"),
			highlighted : false,
			reviews : 15,
			points : 45,
			rating: 3,
			keywords : [ "Mixed Bag of Surprises","Tampere","onions", "rolls"],
			__v : 0 
		},
		{ 
			name : "Pickled Fried Herring",
			description : "Very salty, fried herring. A traditional Finnish food.",
			price : 7,
			type : "Elixir",
			photopath : "prototype_burger.jpg",
			restaurant : ObjectId("531ee47e6e8942592fcb257c"),
			_id : ObjectId("5321b750a1239ed6e06eb387"),
			updated : ISODate("2014-03-13T13:49:04.553Z"),
			added : ISODate("2014-03-13T13:49:04.553Z"),
			highlighted : false,
			reviews : 22,
			points : 88,
			rating: 4,
			keywords : [ "Mixed Bag of Surprises","Tampere","leipajuusto", "bread cheese", "dessert"],
			__v : 0 
		}
	];


	var	reviews = [
		{ "dish" : ObjectId("5321b3d9ea4b9b2bde723c2f"), "rating" : 5, "text" : "Absolutelu brilliant. I ate one of these sausages and I was full for the next four days.", "user" : ObjectId("5321b9287bbf2ea6f8c45521"), "_id" : ObjectId("5321bb5a3b42a0cbe793ea0e"), "updated" : ISODate("2014-03-13T14:06:18.574Z"), "added" : ISODate("2014-03-13T14:06:18.573Z"), "__v" : 0 },
		{ "dish" : ObjectId("5321b3d9ea4b9b2bde723c2f"), "rating" : 1, "text" : "Horrifying!. Utterly horrifying dish.", "user" : ObjectId("5321b9287bbf2ea6f8c45521"), "_id" : ObjectId("5321bbd43b42a0cbe793ea0f"), "updated" : ISODate("2014-03-13T14:08:20.356Z"), "added" : ISODate("2014-03-13T14:08:20.356Z"), "__v" : 0 },
		{ "dish" : ObjectId("5321b3d9ea4b9b2bde723c2f"), "rating" : 4, "text" : "I like blood and I like sausage. This is the perfect food for me.", "user" : ObjectId("5321b9287bbf2ea6f8c45521"), "_id" : ObjectId("5321bdce3b42a0cbe793ea10"), "updated" : ISODate("2014-03-13T14:16:46.917Z"), "added" : ISODate("2014-03-13T14:16:46.917Z"), "__v" : 0 },
	];


	// Save the seed data
	db.users.save(users);
	db.dishes.save(dishes);
	db.reviews.save(reviews);
	
	


